#include <stdint.h>

namespace messages {
const uint64_t power = 0x4CB3817E;
const uint64_t hdmi = 0x4CB3619E;
const uint64_t volup = 0x4CB331CE;
const uint64_t voldown = 0x4CB3F10E;
} // namespace messages
