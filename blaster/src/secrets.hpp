#ifndef SECRETS_HPP
#define SECRETS_HPP

extern const char *password;
extern const char *ssid;
extern const char *auth_key;
extern const char *root_ca;

#endif