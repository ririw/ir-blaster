#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>

#include <IRremoteESP8266.h>
#include <IRsend.h>

#include "messages.hpp"
#include "secrets.hpp"

const int iRPin = 4;
IRsend irsend(iRPin);

void setup() {
  Serial.begin(9600);
  WiFi.begin(ssid, password);
  irsend.begin();

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print("*");
  }

  Serial.println("Connected to network");
}

void loop() {
  /*
  irsend.sendNEC(messages::power);
  Serial.println("POWER");
  delay(500);
  return;
  */

  std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
  client->setInsecure();
  HTTPClient https;
  https.setTimeout(20000);
  https.begin(*client, "https://ir-blaster.dev.richardweiss.net/pull/1");
  https.addHeader("Auth", auth_key);
  int httpResponseCode = https.GET();
  String payload = https.getString();
  if (httpResponseCode != 200) {
    Serial.println("Request failed: ");
    Serial.println(httpResponseCode);
    Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpResponseCode).c_str());
    Serial.println(payload);
    delay(5000);
  } else {
    Serial.print(payload);
    Serial.println("<");
    if (payload == "power") {
      irsend.sendNEC(messages::power);
      delay(500);
      irsend.sendNEC(messages::power);
      Serial.println("power");
    } else if (payload == "hdmi") {
      irsend.sendNEC(messages::hdmi);
      Serial.println("HDMI");
    } else if (payload == "volup") {
      irsend.sendNEC(messages::volup);
      Serial.println("Volup");
    } else if (payload == "voldown") {
      irsend.sendNEC(messages::voldown);
      Serial.println("Voldown");
    } else if (payload == "none") {
      Serial.println("No action");
    } else {
      Serial.println("Unknown message");
      Serial.print(payload);
      Serial.println("<");
      delay(5000);
    }
  }
}