use std::sync::{Arc, mpsc, Mutex};
use anyhow::{Context, Result};
use log::warn;

#[derive(Debug, Copy, Clone)]
pub enum IRMsg {
    PowerToggle,
    HDMIChan,
    VolUp,
    VolDown
}

#[derive(Debug, Clone)]
pub struct Subscription {
    id: i32, 
    sender: Arc<mpsc::SyncSender<IRMsg>>,
    receiver: Arc<Mutex<mpsc::Receiver<IRMsg>>>,
}

impl Subscription {
    pub fn new(id: i32) -> Self {
        let (sender, receiver) = mpsc::sync_channel(16);

        Subscription {
            id,
            sender: Arc::new(sender),
            receiver: Arc::new(Mutex::new(receiver)),
        }
    }

    pub fn send(&self, msg: IRMsg) -> Result<()> {
        if let Err(e) = self.sender.try_send(msg) {
            warn!("Had trouble pushing data - got error {:?}", e);
            self.recv(std::time::Duration::from_nanos(1));
            self.sender.try_send(msg).context("Atempted to send again but failed").unwrap()
        }
        Ok(())
    }

    pub fn recv(&self, timeout: std::time::Duration) -> Option<IRMsg> {
        let recv = self.receiver.lock().unwrap();
        match recv.recv_timeout(timeout) {
            Ok(msg) => Some(msg),
            Err(_) => None
        }
    }
}

