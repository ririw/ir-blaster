use std::collections::HashMap;
use ir_blaster::logic::{Subscription, IRMsg};
use std::env;
use std::net::{IpAddr};
use warp::http::Response;
use warp::Filter;
use log::{info, warn};


fn handle_message(key: &Option<String>, logic: &Subscription, auth: &Option<String>, message: IRMsg) -> Response<&'static str> {
    if key.is_some() && key != auth {
        warn!("Unauthorised request");
        Response::builder()
            .status(401)
            .body("Unauthorised")
            .unwrap()
    } else {
        info!("Got message: {:?}", message);
        logic.send(message).unwrap();
        Response::builder()
            .status(200)
            .body("Ok")
            .unwrap()
    }
}

fn handle_pull(key: &Option<String>, logic: &Subscription, auth: &Option<String>) -> Response<&'static str> {
    if key.is_some() && key != auth {
        warn!("Unauthorised request");
        Response::builder()
            .status(401)
            .body("Unauthorised")
            .unwrap()
    } else {
        let recv = logic.recv(std::time::Duration::from_secs(5));
        if recv.is_some() {
            info!("Sending back: {:?}", recv);
        }
        match recv {
            None => Response::builder()
                .status(200)
                .body("none")
                .unwrap(),
            Some(IRMsg::PowerToggle) => Response::builder()
                .status(200)
                .body("power")
                .unwrap(),
            Some(IRMsg::HDMIChan) => Response::builder()
                .status(200)
                .body("hdmi")
                .unwrap(),
            Some(IRMsg::VolUp) => Response::builder()
                .status(200)
                .body("volup")
                .unwrap(),
            Some(IRMsg::VolDown) => Response::builder()
                .status(200)
                .body("voldown")
                .unwrap(),

        }
    }
}


#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    let addr: IpAddr = if cfg!(debug_assertions) {
        "127.0.0.1".parse().unwrap()
    } else {
        "0.0.0.0".parse().unwrap()
    };

    let port = match env::var("PORT") {
        Ok(p) => p.parse::<u16>().unwrap(),
        Err(..) => 8000,
    };

    let key: Option<String> = env::var("KEY").map(Some).unwrap_or(None);
    let logic = Subscription::new(0);

    let autharg = warp::query::<HashMap<String, String>>().map(move |a: HashMap<String, String>| a.get("auth").map(|a| a.to_owned()));
    let authheader = warp::header::<String>("auth").map(Some);
    // Favour the header over the arg - this is because
    // the arg will always be something (ie, None) rather
    // than a warp error
    let _key = key.clone(); 
    let authfilter = authheader.or(autharg).unify();


    let _key = key.clone(); let _logic = logic.clone();
    let power = warp::path!("msg" / "power" / "1").and(authfilter).map(move |auth| handle_message(&_key, &_logic, &auth, IRMsg::PowerToggle));
    let _key = key.clone(); let _logic = logic.clone();
    let chan = warp::path!("msg" / "channel" / "1").and(authfilter).map(move |auth| handle_message(&_key, &_logic, &auth, IRMsg::HDMIChan));
    let _key = key.clone(); let _logic = logic.clone();
    let volup = warp::path!("msg" / "volup" / "1").and(authfilter).map(move |auth| handle_message(&_key, &_logic, &auth, IRMsg::VolUp));
    let _key = key.clone(); let _logic = logic.clone();
    let louder = warp::path!("msg" / "louder" / "1").and(authfilter).map(move |auth| handle_message(&_key, &_logic, &auth, IRMsg::VolUp));
    let _key = key.clone(); let _logic = logic.clone();
    let quieter = warp::path!("msg" / "quieter" / "1").and(authfilter).map(move |auth| handle_message(&_key, &_logic, &auth, IRMsg::VolDown));
    let _key = key.clone(); let _logic = logic.clone();
    let voldown = warp::path!("msg" / "voldown" / "1").and(authfilter).map(move |auth| handle_message(&_key, &_logic, &auth, IRMsg::VolDown));

    let log = warp::log::custom(|info| {
        // Use a log macro, or slog, or println, or whatever!
        info!(
            "{} {:?} {}",
            info.method(),
            info.request_headers(),
            info.path(),
        );
    });
    let route = warp::any()
        .map(warp::reply)
        .with(log);

    let _key = key.clone(); let _logic = logic.clone();
    let pull = warp::path!("pull" / "1").and(authfilter).map(move |auth| handle_pull(&_key, &logic, &auth)).with(log);

    let ping = warp::path!("ping" / String).map(|name| format!("pong - {}", name));

    warp::serve(
        ping
        .or(power)
        .or(pull)
        .or(chan)
        .or(volup)
        .or(louder)
        .or(voldown)
        .or(quieter)
    ).run((addr, port)).await;
}